package com.example.tsscope;

import java.util.Arrays;

public class AudioProcessor {
    private int dBFS(int value) {
        int val = value;

        if (val == 0)
            val = 1;
        if (val == -32768)
            val = -32767;

        int valueDBFS = (int)(20.0 * Math.log10(Math.abs(val)/32768.0));
        valueDBFS += 90;
        //System.out.println("val " + valueDBFS);
        return valueDBFS;

    }
    int [] calculateAmplitude(AudioBuffer audioBuffer) {
        int[] buffer = audioBuffer.getAudioBuffer();
        int [] tmpBuffer = new int[32];
        int [] amplitudeBuffer = new int[buffer.length / 32];

        for (int i = 0, index = 0; i < buffer.length; i += 32, index++) {
            System.arraycopy(buffer, i, tmpBuffer, 0, 32);
            int min = Arrays.stream(tmpBuffer).min().getAsInt();
            int max = Arrays.stream(tmpBuffer).max().getAsInt();
            //System.out.println("min " + min + " max " + max);

            amplitudeBuffer[index] = dBFS((min + max) / 2);
        }
        return amplitudeBuffer;
    }
}
