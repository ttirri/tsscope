package com.example.tsscope;

import java.util.Arrays;

public class FFT {
    private int SAMPLE_RATE = 4000;

    private int FFT_SIZE = 1024;
    private int FFT_SHIFT = 512;
    private int FFT_BITS = 10;
    public int MAX_VALUE = 8192;
    private int DB_MULTIPLIER = 20;

    //60 for JefFFT
    private int DB_OFFSET = 50;

    private double [] mDataImag;
    private double [] mWindow;
    private double [] mFreqTable;
    private double [] mLatestFFTResult;
    private FFTImpl mFFT;

    public FFT() {
        mDataImag = new double[FFT_SIZE];
        mWindow = new double[FFT_SIZE];
        mFreqTable = new double[FFT_SIZE];
        mLatestFFTResult = new double[FFT_SIZE];

        for( int i = 0; i < FFT_SIZE; ++i ) {
            mFreqTable[i] = ( SAMPLE_RATE * i ) / (float) ( FFT_SIZE );
            mDataImag[i] = 0.0f;
            mWindow[i] = 0.5f * ( 1.0f - Math.cos( 2.0 * Math.PI * i / (FFT_SIZE - 1.0) ) );
        }

        //mFFT = new JefFFT(FFT_BITS);
        mFFT = new nayukiFFT(FFT_BITS);
    }

    private void applyWindow( double [] window, double []data, int size ) {
        for( int i = 0; i < size; ++i )
            data[i] *= window[i];
    }

    double [] performFFT(AudioBuffer audioBuffer) {
        //32 bytes comes each time, 125 frames per second
        boolean ret = false;

        double [] signalBuffer = audioBuffer.getFFTBuffer(FFT_SIZE);
        audioBuffer.progressFFTPos(FFT_SHIFT);

        applyWindow(mWindow, signalBuffer, FFT_SIZE);
        clearBuffer(mDataImag);

        mFFT.doFFT( signalBuffer, mDataImag );
        calculateFFTResult(signalBuffer, mDataImag);

        return getLatestFFTResult();
    }

    private void calculateFFTResult(double[] real, double[] imag) {
        double value;
        double sum = Arrays.stream(mWindow).sum();

        for (int i = 0; i < mLatestFFTResult.length; i++) {
            value = Math.pow(real[i],2) + Math.pow(imag[i], 2);
            value = Math.sqrt(value);
            value = (value * 2) / sum;
            mLatestFFTResult[i] = DB_MULTIPLIER * Math.log10(value);
            mLatestFFTResult[i] += DB_OFFSET;
        }
        /*double min = Arrays.stream(mLatestFFTResult).min().getAsDouble();
        double max = Arrays.stream(mLatestFFTResult).max().getAsDouble();
        System.out.println("max " + max + " " + min);*/
    }

    private void clearBuffer(double[] buffer) {
        for (int i = 0; i < buffer.length; i++) {
            buffer[i] = 0.0;
        }
    }

    public double [] getLatestFFTResult() {
      return mLatestFFTResult.clone();
    }

    public double [] getFrequencyTable() {
        return mFreqTable.clone();
    }

    public int getFFTSize() { return FFT_SIZE; }
}
