package com.example.tsscope;

class SecondOrderHighPassButtersworthFilter {
    private static float q = 0.7071f;
    private float a0r;
    private float a1;
    private float a2;
    private float b0;
    private float b1;
    private float b2;
    private float x1 = 0.0f;
    private float x2 = 0.0f;

    SecondOrderHighPassButtersworthFilter(float f, float f2) {
        double d = (double) ((f2 / f) * 6.2831855f);
        float cos = (float) Math.cos(d);
        float sin = ((float) Math.sin(d)) * ((float) Math.sinh((double) (0.5f / q)));
        this.a1 = -2.0f * cos;
        this.a2 = 1.0f - sin;
        this.b1 = -1.0f - cos;
        this.b0 = this.b1 * -0.5f;
        this.b2 = this.b0;
        this.a1 = -this.a1;
        this.a2 = -this.a2;
        this.a0r = 1.0f / (sin + 1.0f);
    }

    public float filterSample(float f) {
        float f2 = this.a0r;
        float f3 = this.x1;
        float f4 = this.x2;
        float f5 = f + (this.a1 * f2 * f3) + (this.a2 * f2 * f4);
        float f6 = (this.b0 * f5 * f2) + (this.b1 * f2 * f3) + (f2 * this.b2 * f4);
        this.x2 = f3;
        this.x1 = f5;
        return f6;
    }

    public void reset() {
        this.x1 = 0.0f;
        this.x2 = 0.0f;
    }
}