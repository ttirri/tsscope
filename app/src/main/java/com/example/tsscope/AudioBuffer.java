package com.example.tsscope;

import java.util.Arrays;

public class AudioBuffer {
    private int mAudioData[];
    private int BUFFER_SIZE = 8 * 1024;

    private int mCurPos = 0;
    private int mFFTPos = 0;
    private int mAudioPos = 0;


    public AudioBuffer() {
        mAudioData = new int[BUFFER_SIZE]; //TODO Check len
        for (int i = 0; i < BUFFER_SIZE; i++) {
            mAudioData[i] = 0;
        }
        mCurPos = 0;
        mFFTPos = 0;
        mAudioPos = 0;
    }

    //ring buffer
    synchronized public void addData(int[] signalData) {
        System.arraycopy(signalData, 0, mAudioData, mCurPos, signalData.length);
        mCurPos += signalData.length;

        if (mCurPos >= BUFFER_SIZE)
            mCurPos = 0;
        int min = Arrays.stream(signalData).min().getAsInt();
        int max = Arrays.stream(signalData).max().getAsInt();
        //System.out.println("min " + min + " max " + max);
    }

    synchronized public  double[] getFFTBuffer(int size) {
        double []buffer = new double[size];
        for (int i = 0; i < size; i++) {
            int index = (mFFTPos + i)%BUFFER_SIZE;
            buffer[i] = (double)mAudioData[index];
        }

        return buffer;
    }

    synchronized public void progressFFTPos(int shift) {
        mFFTPos = (mFFTPos + shift)%BUFFER_SIZE;
    }

    synchronized public boolean readyForFFT(int fft_size) {
        int size = mCurPos - mFFTPos;
        if (size < 0)
            size = BUFFER_SIZE - mFFTPos + mCurPos;

        return (size >= fft_size);
    }

    synchronized public int[] getAudioBuffer() {
        int size = mCurPos - mAudioPos;

        if (size < 0)
            size = BUFFER_SIZE - mAudioPos + mCurPos;

        int []buffer = new int[size];

        for (int i = 0; i < size; i++) {
            int index = (mAudioPos + i)%BUFFER_SIZE;
            buffer[i] = mAudioData[index];
        }
        //int min = Arrays.stream(buffer).min().getAsInt();
        //int max = Arrays.stream(buffer).max().getAsInt();
        //System.out.println("min " + min + " max " + max);
        mAudioPos = mCurPos;

        return buffer;
    }
}
