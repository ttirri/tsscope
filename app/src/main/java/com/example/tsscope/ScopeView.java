package com.example.tsscope;

import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Color;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.Path;

import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import android.os.Build;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class ScopeView extends View {
    private final PorterDuffXfermode mXfermode;
    private final Paint mBitmapPaint;
    //private final Typeface mTf;

    private int mWidth;
    private int mHeight;

    private Bitmap mStaticBitmap;
    private Canvas mStaticCanvas;
    private Bitmap mDynamicBitmap;
    private Canvas mDynamicCanvas;
    private Context mContext;
    private double mSignalValue;
    private int mSignalIndex;
    private double[] mLatestFFTResuls;
    private double[] mLatestFFTResuls1;
    private double[] mLatestFFTResuls2;
    private int index;

    public ScopeView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        mXfermode = new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER);
        mBitmapPaint = new Paint();
        mBitmapPaint.setFilterBitmap(false);

        mContext = context;
        mLatestFFTResuls  = new double[1024];
        mLatestFFTResuls1 = new double[1024];
        mLatestFFTResuls2 = new double[1024];
        index = 0;

        //get size from fft

        //mTf = Typeface.createFromAsset(context.getAssets(),"fonts/D-DIN.ttf");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Point pt = new Point((int) event.getX(), (int) event.getY());
/*        Rect barRect = getBarRect(mDynamicCanvas);

        if (event.getAction() == MotionEvent.ACTION_DOWN && barRect.contains(pt.x, pt.y)) {
            setVasValue(pt, barRect);
            drawDynamic(mDynamicCanvas);
            invalidate();
        } */
        System.out.println(pt.toString());
        return true;
    }

    private void setSignalValue(double value, int index) {
        mSignalValue = value;
        mSignalIndex = index;
    }

    @Override
    public void addChildrenForAccessibility(ArrayList<View> outChildren) {
        super.addChildrenForAccessibility(outChildren);
    }

    private void drawSignalText(Canvas canvas) {
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(Color.RED);
        p.setStrokeWidth(2.0f);
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        //p.setTypeface(mTf);
        p.setTextSize(60.0f);

        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        float width = canvas.getWidth();
        float height = canvas.getHeight();

        String text = String.valueOf(mSignalValue);
        String add = String.valueOf(mSignalIndex);
        text = text + " " + add;

        float x = width / 2.0f;
        float y = height / 2.0f;

        //draw angle text
        Rect textRect = new Rect();
        p.getTextBounds(text, 0, text.length(), textRect);
        x = x - textRect.width() / 2.0f;
        y = y + textRect.height() / 2.0f;

        canvas.drawText(text, x, y, p);
    }

    private void drawFFTCurve(Canvas canvas) {
        Paint p = new Paint();
        p.setAntiAlias(true);
        int angleColor = Color.parseColor("#F9423A");
        p.setColor(angleColor);
        p.setStrokeWidth(1.0f);
        p.setStyle(Paint.Style.STROKE);

        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        float width = canvas.getWidth();
        float height = canvas.getHeight();

        float baseY = height - 96;
        float topY = 96;

        float baseX = 48;

        for (int i = 0; i < 512; i++) {
            canvas.drawLine((float)(baseX + i), baseY, (float)(baseX + i), getYFromFFT(baseY, topY, mLatestFFTResuls[i]),p);
        }
    }

    public int getPixelValueAudio(int i, int h) {
        int i2 = 32767;
        if (i > 32767) {
            i = 32767;
        } else if (i < -32768) {
            i = -32768;
        }
        int i3 = -((h * i) / 6);
        if (i <= 0) {
            i2 = 32768;
        }
        return (i3 / i2) + (h / 2);
    }

    private float getYFromFFT(float baseY, float topY, double fftResult) {
        //fft range 0-100
        float k = (topY - baseY) / 100.0f;
        float y = baseY + (k * ((float) fftResult));

        if (fftResult < 0) {
            System.out.println(fftResult);
        }
        return y;
    }

    private void drawDynamic(Canvas canvas) {

/*
        text = String.valueOf((int)mPitch);
        canvas.drawText(text, x, y + 50, p);

        text = String.valueOf((int)mInclination);
        canvas.drawText(text, x, y + 100, p);
*/
        drawFFTCurve(canvas);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        //System.out.println("w " + left + " " + right);
        //System.out.println("h " + top + " " + bottom );
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mWidth = w;
        mHeight = h;

        if (mWidth != 0 && mHeight != 0 && mStaticBitmap == null) {
            mStaticBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
            mStaticCanvas = new Canvas(mStaticBitmap);
            int bkColor = Color.parseColor(mContext.getString(R.color.scope_background));
            mStaticCanvas.drawColor(bkColor);
        }

        if (mWidth != 0 && mHeight != 0 && mDynamicBitmap == null) {
            mDynamicBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
            mDynamicCanvas = new Canvas(mDynamicBitmap);
            mDynamicCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        }
    }

    private int saveLayer(Canvas canvas) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return canvas.saveLayer(0, 0, mWidth, mHeight, null);
        } else {
            //noinspection deprecation
            return canvas.saveLayer(0, 0, mWidth, mHeight, null, Canvas.ALL_SAVE_FLAG);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int sc = saveLayer(canvas);

        canvas.drawBitmap(mStaticBitmap, 0, 0, mBitmapPaint);
        mBitmapPaint.setXfermode(mXfermode);

        canvas.drawBitmap(mDynamicBitmap, 0, 0, mBitmapPaint);
        mBitmapPaint.setXfermode(null);

        canvas.restoreToCount(sc);
    }

    public void handleFFTResult(double[] fftValues) {
        //TODO: average here
        //TODO: maxhold here
        if (index == 0) {
            System.arraycopy(fftValues, 0, mLatestFFTResuls1, 0, fftValues.length);
            index = 1;
        }
        else {
            System.arraycopy(fftValues, 0, mLatestFFTResuls2, 0, fftValues.length);
            index = 0;
        }
        for (int i = 0; i < mLatestFFTResuls.length; i++) {
            mLatestFFTResuls[i] = (mLatestFFTResuls1[i] + mLatestFFTResuls2[i]) / 2;
        }


        drawDynamic(mDynamicCanvas);
        invalidate();
    }

    private void handleRawAudio(short[] data) {
        for (int i = 0; i < 1024; i++) {
            mLatestFFTResuls[i] = (double) data[i];
        }
        drawDynamic(mDynamicCanvas);
        invalidate();
    }
}
//split to scopeview and audioview
