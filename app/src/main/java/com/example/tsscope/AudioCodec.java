package com.example.tsscope;

public class AudioCodec {
    private static final byte[] ima_index_table = {
            -1, -1, -1, -1, 2, 4, 6, 8, -1, -1, -1, -1, 2, 4, 6, 8
    };

    private static final short[] ima_step_table = {
            7, 8, 9, 10, 11, 12, 13, 14, 16, 17,
            19, 21, 23, 25, 28, 31, 34, 37, 41, 45,
            50, 55, 60, 66, 73, 80, 88, 97, 107, 118,
            130, 143, 157, 173, 190, 209, 230, 253, 279, 307,
            337, 371, 408, 449, 494, 544, 598, 658, 724, 796,
            876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066,
            2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358,
            5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899,
            15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767
    };

    private int predictor;
    private int step_index;

    public AudioCodec() {
        this.predictor = 0;
        this.step_index = 0;
    }

    public void reset() {
        this.predictor = 0;
        this.step_index = 0;
    }

    /*
        Decode count samples of ADPCM to 16-bit stereo little-endian PCM audio data.
    */
    public byte[] decode( byte[] bArr ) {
         byte[] bArr2 = new byte[(bArr.length * 4)];
         int i = 0;

         for (byte decodeByte : bArr) {
             byte[] decodeByte2 = decodeByte(decodeByte);
             int i2 = i * 4;

             bArr2[i2] = decodeByte2[0];
             bArr2[i2 + 1] = decodeByte2[1];
             bArr2[i2 + 2] = decodeByte2[2];
             bArr2[i2 + 3] = decodeByte2[3];
             i++;
         }
         return bArr2;
    }

    private byte[] decodeByte(byte b) {
        byte[] bArr = new byte[4];

        for (int i : new int[]{0, 1}) {
            byte b2 = (byte) (b >> ((1 - i) * 4));
            short s = ima_step_table[this.step_index];
            int i2 = s >> 1;
            if ((b2 & 4) != 0) {
                i2 += s << 2;
            }
            if ((b2 & 2) != 0) {
                i2 += s << 1;
            }
            if ((b2 & 1) != 0) {
                i2 += s;
            }
            int i3 = i2 >> 2;
            if ((b2 & 8) != 0) {
                this.predictor -= i3;
            } else {
                this.predictor += i3;
            }
            int i4 = this.predictor;
            if (i4 > 32767) {
                this.predictor = 32767;
            } else if (i4 < -32768) {
                this.predictor = -32768;
            }
            this.step_index += ima_index_table[b2 & 15];
            int i5 = this.step_index;
            if (i5 < 0) {
                this.step_index = 0;
            } else {
                short[] sArr = ima_step_table;
                if (i5 > sArr.length - 1) {
                    this.step_index = sArr.length - 1;
                }
            }
            int i6 = i * 2;
            int i7 = this.predictor;
            bArr[i6 + 1] = (byte) (i7 >> 8);
            bArr[i6] = (byte) i7;
        }
        return bArr;
    }
}
