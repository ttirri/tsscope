package com.example.tsscope;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class AudioView extends View {
    private final PorterDuffXfermode mXfermode;
    private final Paint mBitmapPaint;
    //private final Typeface mTf;

    private int mWidth;
    private int mHeight;

    private Bitmap mStaticBitmap;
    private Canvas mStaticCanvas;
    private Bitmap mDynamicBitmap;
    private Canvas mDynamicCanvas;
    private Context mContext;
    private int[] mLatestAudioResuls;
    private int index;

    public AudioView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        mXfermode = new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER);
        mBitmapPaint = new Paint();
        mBitmapPaint.setFilterBitmap(false);

        mContext = context;
        mLatestAudioResuls  = new int[1024];
        index = 0;

        //get size from fft

        //mTf = Typeface.createFromAsset(context.getAssets(),"fonts/D-DIN.ttf");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Point pt = new Point((int) event.getX(), (int) event.getY());
/*        Rect barRect = getBarRect(mDynamicCanvas);

        if (event.getAction() == MotionEvent.ACTION_DOWN && barRect.contains(pt.x, pt.y)) {
            setVasValue(pt, barRect);
            drawDynamic(mDynamicCanvas);
            invalidate();
        } */
        System.out.println(pt.toString());
        return true;
    }


    @Override
    public void addChildrenForAccessibility(ArrayList<View> outChildren) {
        super.addChildrenForAccessibility(outChildren);
    }

    private void drawAudioCurve(Canvas canvas) {
        Paint p = new Paint();
        p.setAntiAlias(true);
        int angleColor = Color.parseColor("#F9423A");
        p.setColor(angleColor);
        p.setStrokeWidth(1.0f);
        p.setStyle(Paint.Style.STROKE);

        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        float width = canvas.getWidth();
        float height = canvas.getHeight();

        float baseY = height - 96;
        float topY = 96;

        float baseX = 48;

        for (int i = 0; i < 512; i++) {
            canvas.drawLine((float)(baseX + i), baseY, (float)(baseX + i), getYFromValue(baseY, topY, mLatestAudioResuls[i]),p);
        }
    }


    private float getYFromValue(float baseY, float topY, int value) {
        //fft range 0 - 90, 90 -> topY, 0 -> baseY
        float k = (topY - baseY) / 90.0f;
        float y = baseY + (k * ((float) value));

        return y;
    }

    private void drawDynamic(Canvas canvas) {

/*
        text = String.valueOf((int)mPitch);
        canvas.drawText(text, x, y + 50, p);

        text = String.valueOf((int)mInclination);
        canvas.drawText(text, x, y + 100, p);
*/
        drawAudioCurve(canvas);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        //System.out.println("w " + left + " " + right);
        //System.out.println("h " + top + " " + bottom );
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mWidth = w;
        mHeight = h;

        if (mWidth != 0 && mHeight != 0 && mStaticBitmap == null) {
            mStaticBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
            mStaticCanvas = new Canvas(mStaticBitmap);
            int bkColor = Color.parseColor(mContext.getString(R.color.scope_background));
            mStaticCanvas.drawColor(bkColor);
        }

        if (mWidth != 0 && mHeight != 0 && mDynamicBitmap == null) {
            mDynamicBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
            mDynamicCanvas = new Canvas(mDynamicBitmap);
            mDynamicCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        }
    }

    private int saveLayer(Canvas canvas) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return canvas.saveLayer(0, 0, mWidth, mHeight, null);
        } else {
            //noinspection deprecation
            return canvas.saveLayer(0, 0, mWidth, mHeight, null, Canvas.ALL_SAVE_FLAG);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int sc = saveLayer(canvas);

        canvas.drawBitmap(mStaticBitmap, 0, 0, mBitmapPaint);
        mBitmapPaint.setXfermode(mXfermode);

        canvas.drawBitmap(mDynamicBitmap, 0, 0, mBitmapPaint);
        mBitmapPaint.setXfermode(null);

        canvas.restoreToCount(sc);
    }

    public void handleAudioData(int[] data) {
        if (index >= 512)
            index = 0;

        if (index == 0)
            clearAudioResults();

        for (int i = 0; i < data.length; i++) {
            mLatestAudioResuls[index] = data[i];
            index++;
        }
        drawDynamic(mDynamicCanvas);
        invalidate();
    }

    private void clearAudioResults() {
        for (int i = 0; i < mLatestAudioResuls.length; i++) {
            mLatestAudioResuls[i] = 0;
        }
    }
}

