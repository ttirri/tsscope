package com.example.tsscope;

class FilterDelegate {
    private SecondOrderHighPassButtersworthFilter filter0;
    private SecondOrderHighPassButtersworthFilter filter1;
    private SecondOrderHighPassButtersworthFilter filter2;
    private SecondOrderHighPassButtersworthFilter filter3;
    private SecondOrderLowPassButtersworthFilter filter4;
    private SecondOrderLowPassButtersworthFilter filter5;
    private SecondOrderLowPassButtersworthFilter filter6;
    private SecondOrderLowPassButtersworthFilter filter7;
    private boolean filterEnabled;
    private int mAudioAmplitudeScaleFactor = 1;


    FilterDelegate(float f, float f2, float f3) {
       this.filter0 = new SecondOrderHighPassButtersworthFilter(f, f2);
        this.filter1 = new SecondOrderHighPassButtersworthFilter(f, f2);
        this.filter2 = new SecondOrderHighPassButtersworthFilter(f, f2);
        this.filter3 = new SecondOrderHighPassButtersworthFilter(f, f2);
        this.filter4 = new SecondOrderLowPassButtersworthFilter(f, f3);
        this.filter5 = new SecondOrderLowPassButtersworthFilter(f, f3);
        this.filter6 = new SecondOrderLowPassButtersworthFilter(f, f3);
        this.filter7 = new SecondOrderLowPassButtersworthFilter(f, f3);
    }

    /* access modifiers changed from: package-private */
    public void setAudioAmplitudeScaleFactor(int i) {
        if (i > 0) {
            this.mAudioAmplitudeScaleFactor = i;
        }
    }

    public int getAudioAmplitudeScaleFactor() {
        return this.mAudioAmplitudeScaleFactor;
    }

    public void reset() {
        filter0.reset();
        filter1.reset();
        filter2.reset();
        filter3.reset();
        filter4.reset();
        filter5.reset();
        filter6.reset();
        filter7.reset();
    }

    public int filterSample(int i) {
        float f;
        float value = (float)i;

        f = filter7.filterSample(filter6.filterSample(filter5.filterSample(filter4.filterSample(filter3.filterSample(filter2.filterSample(filter1.filterSample(filter0.filterSample(value))))))));

        return (int) (f * ((float) this.mAudioAmplitudeScaleFactor));
    }
}