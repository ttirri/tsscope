package com.example.tsscope;

public interface FFTImpl {
    void doFFT(double [] real, double [] imag);
}
