package com.example.tsscope;

public class SecondOrderLowPassButtersworthFilter {
    private float coefficientFive;
    private float coefficientFour;
    private float coefficientOne;
    private float coefficientThree;
    private float coefficientTwo;
    private float cutOffFrequency;
    private float sampleRateCoefficient;
    private float x1;
    private float x2;

    SecondOrderLowPassButtersworthFilter(float f, float f2) {
        if (f < 0.0f || f2 < 0.0f || f2 > f / 2.0f) {
            this.cutOffFrequency = 800.0f;
            this.sampleRateCoefficient = 2.5E-4f;
        } else {
            this.cutOffFrequency = f2;
            this.sampleRateCoefficient = 1.0f / f;
        }
        double d = (double) ((float) (((double) this.cutOffFrequency) * 6.283185307179586d * ((double) this.sampleRateCoefficient)));
        float cos = (float) Math.cos(d);
        float sin = (float) (((double) ((float) Math.sin(d))) * Math.sinh((double) 0.70711356f));
        float f3 = -2.0f * cos;
        float f4 = 1.0f - cos;
        float f5 = 1.0f / (sin + 1.0f);
        this.coefficientOne = (-f3) * f5;
        this.coefficientTwo = (-(1.0f - sin)) * f5;
        float f6 = 0.5f * f4 * f5;
        this.coefficientThree = f6;
        this.coefficientFour = f4 * f5;
        this.coefficientFive = f6;
    }

    public float filterSample(float f) {
        float f2 = this.coefficientOne;
        float f3 = this.x1;
        float f4 = f + (f2 * f3);
        float f5 = this.coefficientTwo;
        float f6 = this.x2;
        float f7 = f4 + (f5 * f6);
        float f8 = (this.coefficientThree * f7) + (this.coefficientFour * f3) + (f6 * this.coefficientFive);
        this.x2 = f3;
        this.x1 = f7;
        return f8;
    }

    public void reset() {
        this.x1 = 0.0f;
        this.x2 = 0.0f;
    }
}