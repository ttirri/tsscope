package com.example.tsscope;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {
    private ScopeView scopeView;
    private AudioView audioView;
    private StethoscopeConnector scopeConnector;
    private AudioCodec mAudioCodec;
    private AudioProcessor mAudioProcessor;
    private FFT fft;
    private AudioBuffer mAudioBuffer;

    BluetoothManager btManager;
    BluetoothAdapter btAdapter;

    private final static int REQUEST_ENABLE_BT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();

        if (btAdapter != null && !btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }

        mAudioCodec = new AudioCodec();
        mAudioProcessor = new AudioProcessor();
        mAudioBuffer = new AudioBuffer();
        fft = new FFT();

        scopeView = findViewById(R.id.scope_indicator);
        audioView = findViewById(R.id.audio_indicator);

        scopeConnector = new StethoscopeConnector(this.getApplicationContext(), mHandler, btAdapter);
        scopeConnector.startScanning();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case Contants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    handleStethoscopePacket(readBuf);
                    break;
                case Contants.MESSAGE_FFT:
                    double[] fftResult = (double[])msg.obj;
                    scopeView.handleFFTResult(fftResult);
            }
        }
    };

    private int[] decodeStethoscopePacket(byte []array) {
        byte[] decode = mAudioCodec.decode(array);
        int[] resultArray = new int[(decode.length / 2)];

        for (int i3 = 0; i3 < decode.length / 2; i3++) {
            int i4 = i3 * 2;
            int i5 = i4 + 1;
            short s = (short) (((decode[i5] & 255) << 8) | (decode[i4] & 255));
            resultArray[i3] = (int)s;
        }
        return resultArray;
    }

    private void handleStethoscopePacket(byte []array) {
        int []audioArray = decodeStethoscopePacket(array);

        mAudioBuffer.addData(audioArray);
        int[] audioAmplitude = mAudioProcessor.calculateAmplitude(mAudioBuffer);
        audioView.handleAudioData(audioAmplitude);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                if (mAudioBuffer.readyForFFT(fft.getFFTSize())) {
                    double[] fftResult = fft.performFFT(mAudioBuffer);
                    handleFFTResult(fftResult);
                }
            }

            private void handleFFTResult(double[] fftResult) {
                mHandler.obtainMessage(Contants.MESSAGE_FFT, fftResult.length, -1, fftResult).sendToTarget();
            }
        });
    }
}
