package com.example.tsscope;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class StethoscopeConnector {
    private Context mContext;

    private ArrayList<BluetoothDevice> devicesDiscovered = new ArrayList<BluetoothDevice>();
    private BluetoothAdapter mBtAdapter;
    private BluetoothLeScanner btScanner;
    private BluetoothGatt bluetoothGatt;
    private Handler mDataHandler;
    BluetoothGattDescriptor mDescriptor;
    BluetoothGattCharacteristic mGattCharacteristic;

    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";

    final public String DATA_SERVICE="5bf6e500-9999-11e3-a116-0002a5d5c51b";
    final public String DATA_SERVICE_CHARACTERISTICS = "ba9c5360-9999-11e3-966f-0002a5d5c51b";
    final public UUID CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    // Stops scanning after 5 seconds.
    private Handler mHandler = new Handler();
    private static final long SCAN_PERIOD = 5000;


    public StethoscopeConnector(Context context, Handler handler, BluetoothAdapter btAdapter) {
        mContext = context;
        mBtAdapter = btAdapter;
        mDataHandler = handler;

        btScanner = mBtAdapter.getBluetoothLeScanner();
    }

    // Device scan callback.
    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            System.out.println("Device Name: " + result.getDevice().getName() + " rssi: " + result.getRssi() + "\n");
            devicesDiscovered.add(result.getDevice());
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
        }
    };

    // Device connect call back
    private final BluetoothGattCallback btleGattCallback = new BluetoothGattCallback() {

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            // this will get called anytime you perform a read or write characteristic operation
            byte[] value=characteristic.getValue();
            //System.out.println("onCharacteristicRead Value len: " + value.length);
            if (value.length == 20) {
                handleAudioPacket(value);
            }
        }

        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
            // this will get called when a device connects or disconnects
            System.out.println(newState);
            switch (newState) {
                case 0:
                    System.out.println("device disconnected\n");
                    break;
                case 2:
                    System.out.println("device connected\n");
                    bluetoothGatt.discoverServices();
                    break;
                default:
                    System.out.println("we encounterned an unknown state, uh oh\n");
                    break;
            }
        }

        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
            // this will get called after the client initiates a 			BluetoothGatt.discoverServices() call
            System.out.println("device services have been discovered\n");
            displayGattServices(bluetoothGatt.getServices());
        }

        @Override
        // Result of a characteristic read operation
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            System.out.println("On characteristics write: "  + status);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            System.out.println("On descriptor write: "  + status);
            if (mDescriptor == descriptor) {
                System.out.println("Write characteristic");
                mGattCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                boolean setValue = mGattCharacteristic.setValue(hexStringToByteArray("80000000"));
                boolean writeCharacteristic = bluetoothGatt.writeCharacteristic(mGattCharacteristic);
            }
        }
    };

    private void handleAudioPacket(byte[] value) {
        byte[] bArr2 = new byte[16];
        int offset = 4; //could be 1 value len is 17

        for (int i2 = 0; i2 < 16; i2++) {
            bArr2[i2] = value[i2 + offset];
        }
        mDataHandler.obtainMessage(Contants.MESSAGE_READ, bArr2.length, -1, bArr2).sendToTarget();
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {

        System.out.println("ACTION DATA AVAILABLE" + characteristic.getUuid());
    }

    public void startScanning() {
        System.out.println("start scanning");
        devicesDiscovered.clear();

        final List<ScanFilter> filters = new ArrayList<>();
        ScanFilter filter = new ScanFilter.Builder().setDeviceAddress("38:6B:0F:64:CB:B9").build();
        filters.add(filter);

        final ScanSettings.Builder builderScanSettings = new ScanSettings.Builder();
        builderScanSettings.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY);
        builderScanSettings.setReportDelay(0);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //btScanner.startScan(filters, builderScanSettings.build(), leScanCallback);
                btScanner.startScan(leScanCallback);
            }
        });

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScanning();
            }
        }, SCAN_PERIOD);
    }

    public void stopScanning() {
        System.out.println("stopping scanning");
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                btScanner.stopScan(leScanCallback);
            }
        });
        connectToDevice();
    }

    private void connectToDevice() {
        System.out.println("Trying to connect to device: " + devicesDiscovered.get(0).getAddress() + "\n");
        int deviceSelected = 0;
        bluetoothGatt = devicesDiscovered.get(deviceSelected).connectGatt(mContext, false, btleGattCallback, BluetoothDevice.TRANSPORT_LE);
    }

    public void disconnectDeviceSelected() {
        System.out.println("Disconnecting from device\n");
        bluetoothGatt.disconnect();
    }

    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {

            final String uuid = gattService.getUuid().toString();
            System.out.println("Service discovered: " + uuid);

            new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic :
                    gattCharacteristics) {

                final String charUuid = gattCharacteristic.getUuid().toString();
                System.out.println("Characteristic discovered for service: " + charUuid);

                if (uuid.equalsIgnoreCase(DATA_SERVICE)) {
                    if (charUuid.equalsIgnoreCase(DATA_SERVICE_CHARACTERISTICS)) {
                        setCharacteristicNotification(bluetoothGatt, gattCharacteristic, true);
                        mGattCharacteristic = gattCharacteristic;
                        //write request 0x12, service+characteristics+UUID 0x2902, 0x0001
                        //write command 0x52 handle 0x0008, service+characteristics, 8000 0000
                    }
                }
            }
        }
    }

    private boolean setCharacteristicNotification(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic characteristic,boolean enable) {
        bluetoothGatt.setCharacteristicNotification(characteristic, enable);
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CHARACTERISTIC_UPDATE_NOTIFICATION_DESCRIPTOR_UUID);
        mDescriptor = descriptor;
        descriptor.setValue(enable ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : new byte[]{0x00, 0x00});
        return bluetoothGatt.writeDescriptor(descriptor); //descriptor write operation successfully started?
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

}
